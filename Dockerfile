FROM amazonlinux:2018.03
RUN curl -sL https://rpm.nodesource.com/setup_6.x | bash -
RUN yum update -y
RUN yum install epel-release -y
RUN yum update -y
RUN ln -fs /usr/bin/python2.7 /usr/bin/python
RUN yum install gcc-c++ make wget python27 python27-pip gcc nodejs git python27-setuptools npm -y
RUN pip install django==1.11

RUN yum install -y java-1.8.0-openjdk-headless.x86_64 java-1.8.0-openjdk-devel.x86_64
RUN wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
RUN sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
RUN yum install -y apache-maven
RUN alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
RUN alternatives --set javac /usr/lib/jvm/java-1.8.0-openjdk.x86_64/bin/javac
